﻿(function() {

    window.onload = async () => {        
        let distribution = await get_distribution();

        localStorage.setItem('turn_number', 0);
        localStorage.setItem('distribution', JSON.stringify(distribution));

    }

    function get_distribution() {
        return $.ajax({
            type: 'GET',
            url: '/api/v1/login',
            success: function(data) {
                return data.distribution_array;
            },
            error: function(jqXHR) {
                console.log('some error happened', jqXHR);
            }
        });
    }    
})();