﻿(function ()
{
    window.onload = async () =>
    {
        let user_unique_identifier = localStorage.getItem('user_unique_key');

        if (user_unique_identifier == null)
        {

            showloader();

            let user_unique_key = await register_user();
            localStorage.setItem('user_unique_key', user_unique_key);

            stoploader();
        }
        else
        {
            continue_user_interaction(user_unique_identifier);
        }
    }

    async function register_user()
    {
        let new_unique_key = generate_unique_id();
        let registration_result = await check_identifier_exist(new_unique_key);

        if (registration_result.is_identifier_unique !== true) {
            await sleep(2000);
            await register_user();
        }

        return new_unique_key;
    }

    function sleep(ms)
    {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    function showloader()
    {
        $('body').loading({
            stoppable: false,
            message: 'Please, wait till your unique id is created'
        });
    }

    function stoploader()
    {
        $('body').loading('stop');
    }

    function check_identifier_exist(identifier)
    {
        return $.ajax({
            type: 'GET',
            url: '/api/v1/register',
            data: { uniqueid: identifier },
            success: function(data) {
                return data.is_identifier_unique;
            },
            error: function(jqXHR) {
                console.log('some error happened', jqXHR);
                $('body').loading('stop');

                window.location.href = '/error';
            }
        });

    }

    function generate_unique_id()
    {
        return Math.random().toString(36).substring(2, 15);
    }

    function continue_user_interaction(identifier)
    {
        return $.ajax({
            type: 'POST',
            url: '/api/v1/register',
            data: { uniqueid: identifier },
            success: function (data) {           
            },
            error: function (jqXHR) {
                console.log('some error happened', jqXHR);
                $('body').loading('stop');

                //window.location.href = '/error';
            }
        });
    }

    //function delete_user_game_data(identifier) {
    //    return $.ajax({
    //        type: 'POST',
    //        url: '/api/v1/register',
    //        data: { uniqueid: identifier },
    //        success: function (data) {           
    //        },
    //        error: function (jqXHR) {
    //            console.log('some error happened', jqXHR);
    //            $('body').loading('stop');

    //            window.location.href = '/error';
    //        }
    //    });
    //}
})();