﻿(function()
{            
    window.should_end_game = false;

    window.addEventListener("beforeunload", function(e) {                   
          window.should_end_game = true;        
    });

    document.addEventListener('visibilitychange', function(e) {                  
           window.should_end_game = true;        
    });  
        
})(jQuery); 