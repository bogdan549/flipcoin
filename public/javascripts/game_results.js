﻿(function() {
    window.onload = async () => {
        let user_unique_key = localStorage.getItem('user_unique_key');
        let result_info = await get_user_result_info(user_unique_key);
        let current_user = result_info.user_result_info[result_info.user_result_info.length-1];
        let parsed_user_game_result = current_user;

        let data_first_chart = [{ y: parsed_user_game_result[0].user_amount, x: 0 }];
        let data_second_chart = [{ y: parsed_user_game_result[0].user_bid, x: 0 }];

        for (var i = 1; i < parsed_user_game_result.length; i++) {
            if (parsed_user_game_result[i].turn_number >= 0) {                
                data_first_chart.push({
                    y: parsed_user_game_result[i].user_amount,
                    x: parsed_user_game_result[i].turn_number
                });
                if (parsed_user_game_result[i].is_choice_right) {
                    data_second_chart.push({
                        y: parsed_user_game_result[i].user_amount == 0 ? 0 : parsed_user_game_result[i].user_bid / parsed_user_game_result[i-1].user_amount * 100,
                        x: parsed_user_game_result[i].turn_number
                    });
                } else {
                    data_second_chart.push({
                        y: parsed_user_game_result[i].user_amount == 0 ? 0 : parsed_user_game_result[i].user_bid / parsed_user_game_result[i-1].user_amount * -100,
                        x: parsed_user_game_result[i].turn_number
                    });
                }
            }
        }

        console.log('first', data_first_chart);
        console.log('first', data_second_chart);

        build_user_chart(d3.select("#visualisation-first-chart"),
            data_first_chart,
            { x: 'User turn', y: 'User capital' });
        build_user_chart(d3.select("#visualisation-second-chart"),
            data_second_chart,
            { x: 'User turn', y: 'user bid/ user capital %' });

        console.log(parsed_user_game_result);
    };

    function build_user_chart(vis, lineData, axis_names) {

        var WIDTH = 500;
        var HEIGHT = 200;
        var MARGINS = {
            top: 20,
            right: 20,
            bottom: 20,
            left: 50
        };

        var xRange = d3.scale.linear().range([MARGINS.left, WIDTH - MARGINS.right]).domain([
            d3.min(lineData,
                function(d) {
                    console.log(d.x);
                    return d.x;
                }),
            d3.max(lineData,
                function(d) {
                    console.log(d.x);
                    return d.x;
                })
        ]);
        var yRange = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([
            d3.min(lineData,
                function(d) {
                    console.log(d.y);
                    return d.y;
                }),
            d3.max(lineData,
                function(d) {
                    console.log(d.y);
                    return d.y;
                })
        ]);
        var xAxis = d3.svg.axis()
            .scale(xRange)            
            .tickSize(5)
            .tickSubdivide(true)
            .tickFormat(function(d) {
              if (d % 1 == 0) {
                return d3.format('.f')(d)
              } else {
                return ""
              }
            });

        var yAxis = d3.svg.axis()
            .scale(yRange)
            .tickSize(5)
            .orient("left")
            .tickSubdivide(true);

        vis.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", WIDTH / 2 + 15)
            .attr("y", 210)
            .text(axis_names.x);

        vis.append("text")
            .attr("class", "y label")
            .attr("text-anchor", "end")
            .attr("y", 6)
            .attr("dy", ".75em")
            .attr("transform", "rotate(-90)")
            .text(axis_names.y);

        vis.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + (HEIGHT - MARGINS.bottom) + ")")
            .call(xAxis);

        vis.append("svg:g")
            .attr("class", "y axis")
            .attr("transform", "translate(" + (MARGINS.left) + ",0)")
            .call(yAxis);

        var lineFunc = d3.svg.line()
            .x(function(d) {
                return xRange(d.x);
            })
            .y(function(d) {
                return yRange(d.y);
            })
            .interpolate('linear');

        vis.append("svg:path")
            .attr("d", lineFunc(lineData))
            .attr("stroke", "blue")
            .attr("stroke-width", 2)
            .attr("fill", "none");
    }

    function get_user_result_info(user_unique_key) {
        return $.ajax({
            type: 'GET',
            url: '/user_result_info',
            data: { user_unique_key: user_unique_key },
            success: function(data) {
                return data;
            },
            error: function(jqHRX) {
                //window.location.href = '/error';
            }
        });
    }
})();