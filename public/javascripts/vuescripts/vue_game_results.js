﻿(function () {
    new Vue({
        el: '#play-again',
        methods: {
            start_new_game() {
                window.innerLinkClicked = true;
                window.location.href = '/';
            }
        }
    }
    );
})();