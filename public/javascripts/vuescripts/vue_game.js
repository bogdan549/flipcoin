﻿(function () {
    Vue.use(VueTables.ClientTable,{
        perPage: 4
    });

    var timer = new Timer();
    var start_game_time = moment().format('LTS');

    const max_game_time = 30;  
    const EndGameReason = {
        user_choice: 'Good game',
        user_loss: 'You are bunkrupt',
        time_is_out: 'Time is out'
    };
    const options = {
            lines: 8, // The number of lines to draw
            length: 15, // The length of each line
            width: 8, // The line thickness
            radius: 40, // The radius of the inner circle
            scale: 0.6, // Scales overall size of the spinner
            corners: 1, // Corner roundness (0..1)
            color: '#ffffff', // CSS color or array of colors
            fadeColor: 'transparent', // CSS color or array of colors
            speed: 1, // Rounds per second
            rotate: 0, // The rotation offset
            animation: 'spinner-line-fade-more', // The CSS animation name for the lines
            direction: 1, // 1: clockwise, -1: counterclockwise
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            className: 'spinner', // The CSS class to assign to the spinner
            top: '0', // Top position relative to parent
            left: '0', // Left position relative to parent
            shadow: '0 0 1px transparent', // Box-shadow for the lines
            position: 'relative' // Element positioning
        };        
    const spinner = new Spinner(options).spin();           

    new Vue({
        el: '#game-controllers',
        data: () => {
            return {
                errors: [],                
                amount: 30,
                bid_value: 0,
                columns: ['bet', 'result', 'total'],
                data: [],
                options: {
                  headings: {
                    bet: 'Bet',
                    result: 'Result',
                    total: 'Total'
                  },
                sortable: ['bet', 'result', 'total']
                }
            }
        },        
        methods:
        {                   
            exit_game() {
                end_game(EndGameReason.user_choice, this.amount);  
            },
            update_game_table(new_game_data)
            {
                this.data.push(new_game_data);         
            },
            validate_user_input()
            {
                if (/[A-Za-z]/.test(this.bid_value) || this.bid_value == 0)
                {
                    this.errors.push('Inserted amount is invalid');
                }
                else
                {
                    if (this.bid_value > this.amount)
                    {
                        this.errors.push(
                            'Your bid amount is higher than your available one, please, choose another bid');
                    };
                }
            },
            async place_bid(bid_type)
            {                
                if (window.should_end_game)
                {
                    end_game(EndGameReason.user_choice);
                    bootbox.alert('Your game is over, please, try again', () => window.location.href = '/');
                }
                else
                {
                    this.errors.length = 0;
                    this.validate_user_input();

                    if (this.errors.length) {
                        bootbox.alert('The input is wrong, please, check error box and try to fix them');
                    }
                    else {
                        showloader();

                        let turn_number = +localStorage.getItem('turn_number');
                        let user_choice_success = JSON.parse(localStorage.getItem('distribution')).distribution_array[turn_number];

                        turn_number++;

                        let is_choice_right = user_choice_success == bid_type;

                        await sleep(3000);

                        let user_amount = is_choice_right
                            ? parseFloat(this.amount) + parseFloat(this.bid_value)
                            : parseFloat(this.amount) - parseFloat(this.bid_value);

                        await post_user_bid({
                            user_unique_key: localStorage.getItem("user_unique_key"),
                            user_amount: user_amount,
                            bid_value: this.bid_value,
                            bid_type: bid_type,
                            is_choice_right: is_choice_right,
                            turn_number: turn_number
                        });

                        stoploader();

                        this.amount = user_amount;

                        let game_info = {
                            bet: this.bid_value,
                            result: is_choice_right ? this.bid_value : '-' + this.bid_value,
                            total: this.amount
                        };

                        this.update_game_table(game_info);
                        this.bid_value = 0;

                        localStorage.setItem('turn_number', turn_number);

                    }
                }
            }
        },                
        mounted() {
            countdown();
        }, 
        watch: {
            amount: (newVal) =>
            {
                if (newVal == 0) {
                    bootbox.alert('Game is over because lost all your money');

                    end_game(EndGameReason.user_loss);                                        
                }
            }
        }
    });

    function post_user_bid(user_bid_data)
    {            
        return $.ajax({
            type: 'POST',
            url: '/api/v1/turn',
            data: user_bid_data,
            success: function(data) {
                console.log(data.success_choice);                
            },
            error: function(jqHRX) {

            }
        });
    }

    function end_game(reason, user_amount)
    {
        let game_time = max_game_time - +$('#game_time').text().split(':')[1];
        console.log(start_game_time);

        $.ajax({
            type: 'get',
            url: '/api/v1/turn',
            data: { user_unique_key: localStorage.getItem("user_unique_key"), game_time: game_time, start_game_time: start_game_time },
            success: function (data) {                                
            },
            error: function (jqHRX) {
            }
        });

        if(!window.should_end_game)
            window.location.href = `/game_results?reason=${reason}&user_amount=${user_amount}`;             
    }

    function countdown() {        

        timer.start({ countdown: true, startValues: { minutes: 30 } });
        $('.countdown #game_time').html(timer.getTimeValues().toString());

        timer.addEventListener('secondsUpdated',
            function (e) {
                $('.countdown #game_time').html(timer.getTimeValues().toString());
            });

        timer.addEventListener('targetAchieved',
            function (e) {
                bootbox.alert('Opps, time is out');
                window.location.href = '/game_results';
            });
    }

    function showloader()
    {
        $('.form-row').css({ 'display': 'none' });

        spinner.spin(document.getElementById('spinner'));
        $(spinner.el).append('<span class="spin_coin">coin is spinning</span>');
    }

    function stoploader()
    {
        spinner.stop();
        $('.form-row').css({ 'display': 'block' });
    }

    function sleep(ms)
    {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
})();