﻿(function () {            
        Vue.component('pagination', Pagination);
        new Vue({
                el: '.admin_info',
                data: () => {
                    return{
                        users_keys: [],
                        users_values: [],
                        games_amount: 0,
                        average_game_time: 0,
                        average_turn_amount: 0,                        
                        current_page: 1,
                        pages_amount: null
                    }
                },
                async mounted() {
                    let response = await get_users_info();

                    let users_array = JSON.parse(response.users);                                        

                    for (let key of Object.keys(users_array)) {
                        this.users_keys.push(key);
                        this.users_values.push([]);
                        let user_index = Object.keys(users_array).indexOf(key);
                      for (let i = 0; i < users_array[key].length; i++)
                        this.users_values[user_index].push(users_array[key][i]);
                    };

                    let turns_amount = 0, times_amount = 0, games_amount = 0;

                    for (let i of this.users_values) 
                        {
                            for (let j of i) 
                               {
                                   for(let z of j)
                                        {
                                          if (z.turn_number > 0)
                                                turns_amount++;
                                          if (z.turn_number == -1)
                                              times_amount += +z.game_time;
                                        }                                                        
                                games_amount++;
                               }                        
                        }

                    console.log(this.users_values);                    

                    let total_length_amount = 0;                    

                    for(let i = 0; i < this.users_values.length; i++)
                    {
                        this.games_amount += this.users_values[i].length;
                        total_length_amount += this.users_values[i].length;                        
                    }

                    this.average_game_time = Math.ceil(times_amount / total_length_amount);
                    this.average_turn_amount = Math.round(turns_amount / total_length_amount);

                    localStorage.setItem('users_values', JSON.stringify(this.users_values));                                                                              

                    build_general_chart(this.users_values, this.users_keys);    
                    
                },
            methods: {
                paginate_charts: function (page_number) {
                    $('.users_charts.active_chart .user_charts').each(function (chart_number) {
                        if (chart_number == page_number - 1) {
                            $(this).css({ 'display': 'block' });
                            console.log($(this));
                        }
                        else {
                            $(this).css({ 'display': 'none' });
                        }
                    });
                },
                setPage: function (page_number) {
                    this.paginate_charts(page_number--);
                },                
              }
            }
        );    

    window.onload = () => {        
        let users_values = JSON.parse(localStorage.getItem('users_values'));        

        build_users_charts(users_values);

        console.log('hey', users_values);
        $('.list-group a').first().addClass('active');
        $('.charts:eq(0)').addClass('active_chart');
        paginate_users_charts();        

        $('.list-group a').on('click', function () {
            show_element($(this).prop('id'));
            $(this).parent().find('a.active').removeClass('active');
            $(this).addClass('active');   

            console.log($(this).index());
            paginate_users_charts();
        });
        
    }

    function paginate_users_charts(user_list_index) {        
        $('.users_charts.active_chart .user_charts').not(':eq(0)').each(function (chart_number) {
            $(this).css({ 'display': 'none' });
        });

        //switch on first page
        $('.users_charts.active_chart .pagination li').eq(2).click();
    }

    function show_element(el_index) {
        $('.charts.active_chart').removeClass('active_chart').addClass('charts');
        $('.charts').eq(el_index).addClass('active_chart');

    }

    function build_users_charts(users_values) {

        let users_charts = document.getElementsByClassName('users_charts');                     

        for (let i = 0; i < users_values.length; i++) {
                for (let j = 0; j < users_values[i].length; j++) 
                {
                    let data_first_chart = [];
                    let data_second_chart = [];
                    let lables = [];
                    
                    for(let k = 0; k < users_values[i][j].length; k++)
                        {                            
                            if (users_values[i][j][k].turn_number >= 0) {
                                data_first_chart.push(
                                   users_values[i][j][k].user_amount
                                );

                                lables.push(users_values[i][j][k].turn_number);

                                if (users_values[i][j][k].is_choice_right)
                                {
                                    data_second_chart.push(users_values[i][j][k].user_amount > 0 ? users_values[i][j][k].user_bid / users_values[i][j][k].user_amount * 100 : 0);
                                }
                                else
                                {
                                    data_second_chart.push(users_values[i][j][k].user_amount > 0 ? users_values[i][j][k].user_bid / users_values[i][j][k].user_amount * -100 : 0);
                                }
                            }
                        }
                    let first_chart_options = {                
                        type: 'line',
                        data: {
                            labels: lables,
                            datasets: [
                                {
                                    label: `${j+1} game`,
                                    data : data_first_chart,
                                    lineTension: 0,
                                    fill: false
                                }
                            ]                
                        },
                        options:{
                            scales: {
                                xAxes:[{
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'user turn'    
                                    }
                                }],
                                yAxes:[{
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'User capital'    
                                    }
                                }]
                            }
                        }
                    }

                    let second_chart_options = {                
                        type: 'line',
                        data: {
                            labels: lables,
                            datasets: [
                                {
                                    label: `${j+1} game`,
                                    data: data_second_chart,
                                    lineTension: 0,
                                    fill: false
                                }
                            ]
                        },
                        options:{
                            scales: {
                                xAxes:[{
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'user turn'    
                                    }
                                }],
                                yAxes:[{
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'user bid/ user capital %'    
                                    }
                                }]
                            }
                        }                        
                    }    
                    new Chart(users_charts[i].childNodes[j].childNodes[0].childNodes[0].childNodes[0], first_chart_options);            
                    new Chart(users_charts[i].childNodes[j].childNodes[0].childNodes[1].childNodes[0], second_chart_options);                                                  
               }            
        }        
    }

    function build_general_chart(users_data, users_keys) {

        let datasets = [];
        let labels = [];
        let subarrays_length = [];

        for (let i = 0; i < users_data.length; i++)
          {            
            let last_user_game = users_data[i].length - 1; 
            subarrays_length.push(users_data[i][last_user_game].length);            
          }

        for (let i = 0; i < Math.max(...subarrays_length); i++)
            labels.push(i);

        for (let i = 0; i < users_data.length; i++) {
            let last_user_game_data = users_data[i].length - 1;
            let data_chart = [];            

            for (let key of Object.keys(users_data[i][last_user_game_data])) {
                if (users_data[i][last_user_game_data][key].turn_number >= 0) {
                    data_chart.push(users_data[i][last_user_game_data][key].user_amount);
                }
            }

            datasets.push({label: users_keys[i], data: data_chart, lineTension: 0, fill: false});            
        }         

        var options = {
            type: 'line',            
            data: {
                labels: labels,
                datasets: datasets
            },
            options:{
                scales: {
                    xAxes:[{
                        scaleLabel: {
                            display: true,
                            labelString: 'user turn'    
                        }
                    }],
                    yAxes:[{
                        scaleLabel: {
                            display: true,
                            labelString: 'User capital'    
                        }
                    }]
                }
            }
        }

        new Chart($('#general_chart'), options);
    }

    function get_users_info() {
        return $.ajax({
            type: 'GET',
            url: '/api/v1/admin_panel_data',
            success: function(data) {
                return JSON.parse(data.users);
            },
            error: function(jqXHR) {
                console.log('some error happened', jqXHR);
            }
        });
    }    
})();