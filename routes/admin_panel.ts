﻿/*
 * GET admin panel.
 */
import express = require('express');
const router = express.Router();

router.get('/', (req: express.Request, res: express.Response) => {
    res.render('admin_panel');
});

export default router;