"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var localstorage = require("localStorage");
var router = express.Router();
router.post('/', function (req, res) {
    var user_unique_key = req.body.user_unique_key;
    JSON.parse(localstorage.getItem('users')[user_unique_key]).length = 0;
    res.send(200);
});
exports.default = router;
//# sourceMappingURL=remove_user_data.js.map