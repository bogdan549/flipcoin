﻿import express = require('express');
import localstorage = require('localStorage');

const router = express.Router();

router.get('/', (req: express.Request, res: express.Response) => {    
    let user_game_result = JSON.parse(localstorage.getItem('users'))[req.query.user_unique_key];

    res.send({ 'user_result_info': user_game_result });
});


export default router;