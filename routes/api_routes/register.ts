﻿import fs = require('fs');
import express = require('express');
import localstorage = require('localStorage');
import { userInfo } from 'os';

const router = express.Router();

router.get('/', (req: express.Request, res: express.Response) => {
    let identifier = req.query.uniqueid;
    let is_identifier_unique = !(identifier in JSON.parse(localstorage.getItem('users')));

    if (is_identifier_unique) {
        var users_object = JSON.parse(localstorage.getItem('users'));
        var file_data = JSON.parse(localstorage.getItem('file_data'));

        let user_info = {
            user_amount: 30,
            user_bid: 0,
            user_choice: 1,
            is_choice_right: true,
            turn_number: 0
        };

        users_object[identifier] = [];
        users_object[identifier].push([]);

        users_object[identifier][0].push(user_info);

        file_data[identifier] = '';


        fs.appendFile('user_files/user_info.csv',`\n ${identifier}`,(err) => {if (err) throw err;});

        localstorage.setItem('users', JSON.stringify(users_object));
        localstorage.setItem('file_data', JSON.stringify(file_data));
    }

    res.status(200);
    res.send({ 'is_identifier_unique': is_identifier_unique });
    
});

router.post('/',(req: express.Request, res: express.Response) => {
    let identifier = req.body.uniqueid;
    let user_array = JSON.parse(localstorage.getItem('users'));    
    let user_info = {
        user_amount: 30,
        user_bid: 0,
        user_choice: 1,
        is_choice_right: true,
        turn_number: 0
    };

    user_array[identifier].push([user_info]);

    localstorage.setItem('users', JSON.stringify(user_array));

    res.send(200);
});

export default router;