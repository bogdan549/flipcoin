"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var localstorage = require("localStorage");
var router = express.Router();
router.get('/', function (req, res) {
    var user_game_result = JSON.parse(localstorage.getItem('users'))[req.query.user_unique_key];
    res.send({ 'user_result_info': user_game_result });
});
exports.default = router;
//# sourceMappingURL=user_result_info.js.map