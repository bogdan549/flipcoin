"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var express = require("express");
var localstorage = require("localStorage");
var router = express.Router();
router.get('/', function (req, res) {
    var identifier = req.query.uniqueid;
    var is_identifier_unique = !(identifier in JSON.parse(localstorage.getItem('users')));
    if (is_identifier_unique) {
        var users_object = JSON.parse(localstorage.getItem('users'));
        var file_data = JSON.parse(localstorage.getItem('file_data'));
        var user_info = {
            user_amount: 30,
            user_bid: 0,
            user_choice: 1,
            is_choice_right: true,
            turn_number: 0
        };
        users_object[identifier] = [];
        users_object[identifier].push([]);
        users_object[identifier][0].push(user_info);
        file_data[identifier] = '';
        fs.appendFile('user_files/user_info.csv', "\n " + identifier, function (err) { if (err)
            throw err; });
        localstorage.setItem('users', JSON.stringify(users_object));
        localstorage.setItem('file_data', JSON.stringify(file_data));
    }
    res.status(200);
    res.send({ 'is_identifier_unique': is_identifier_unique });
});
router.post('/', function (req, res) {
    var identifier = req.body.uniqueid;
    var user_array = JSON.parse(localstorage.getItem('users'));
    var user_info = {
        user_amount: 30,
        user_bid: 0,
        user_choice: 1,
        is_choice_right: true,
        turn_number: 0
    };
    user_array[identifier].push([user_info]);
    localstorage.setItem('users', JSON.stringify(user_array));
    res.send(200);
});
exports.default = router;
//# sourceMappingURL=register.js.map