"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var router = express.Router();
router.get('/', function (req, res) {
    return res.download('user_files/user_info.csv');
});
exports.default = router;
//# sourceMappingURL=download_user_data.js.map