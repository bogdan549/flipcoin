"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var localstorage = require("localStorage");
var router = express.Router();
router.get('/', function (req, res) {
    res.send({ 'users': localstorage.getItem('users') });
});
exports.default = router;
//# sourceMappingURL=admin_panel_data.js.map