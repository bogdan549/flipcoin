﻿import express = require('express');
import localstorage = require('localStorage');
import fs = require('fs');

const router = express.Router();

router.post('/', (req: express.Request, res: express.Response) => {
    let user_unique_key = req.body.user_unique_key;    

    let user_info = {
        user_amount: +req.body.user_amount,
        user_bid: +req.body.bid_value,
        user_choice: +req.body.bid_type,
        is_choice_right: +req.body.is_choice_right,
        turn_number: +req.body.turn_number        
    };    

    let users_array = JSON.parse(localstorage.getItem('users'));
    let user_previous_data = users_array[user_unique_key];
    let user_previous_data_current_item = user_previous_data.length - 1;

    let success_choice = JSON.parse(localstorage.getItem('distribution'))[user_info.turn_number];    

    write_data_to_file(user_unique_key, user_info);
    user_previous_data[user_previous_data_current_item].push(user_info);

    localstorage.setItem('users', JSON.stringify(users_array));    

    res.send({ 'success_choice': success_choice });
});

function write_data_to_file(user_unique_key, user_info) {

    let file_data = JSON.parse(localstorage.getItem('file_data'));
    let data_current_user = file_data[user_unique_key];
    let distribution_value = JSON.parse(localstorage.getItem('distribution'))[user_info.turn_number-1];
    let user_stake_value = user_info.user_bid;

    if (user_info.user_choice == distribution_value) {        
        user_info.is_choice_right = true;
    } else {
        user_stake_value = '-' + user_stake_value;
        user_info.is_choice_right = false;
    }

    data_current_user += ',' + distribution_value + ',' + user_stake_value;
    file_data[user_unique_key] = data_current_user;

    localstorage.setItem('file_data', JSON.stringify(file_data));

    let user_data_file = ',' + distribution_value + ',' + user_stake_value;

    fs.appendFile('user_files/user_info.csv', user_data_file, (err) => {
        if (err)
            throw err;
    });  
}

router.get('/',(req: express.Request, res: express.Response) => {
    let user_unique_key = req.query.user_unique_key;

    let user_info = {
        user_amount: null,
        user_bid: null,
        user_choice: null,
        is_choice_right: null,
        turn_number: -1,
        game_time: req.query.game_time,
        start_game_time: req.query.start_game_time
    };    

    let users_array = JSON.parse(localstorage.getItem('users'));
    let current_user_data = users_array[user_unique_key];
    let user_data_current_item = current_user_data.length - 1;

    current_user_data[user_data_current_item].push(user_info);

    localstorage.setItem('users', JSON.stringify(users_array));    

    res.send(200);
});

export default router;