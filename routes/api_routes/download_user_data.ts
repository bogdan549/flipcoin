﻿import express = require('express');

const router = express.Router();

router.get('/', (req: express.Request, res: express.Response) => {  
    return res.download('user_files/user_info.csv');
});

export default router;