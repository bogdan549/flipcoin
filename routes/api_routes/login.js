"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var localstorage = require("localStorage");
var router = express.Router();
router.get('/', function (req, res) {
    res.status(200);
    res.send({ 'distribution_array': generate_distribution() });
});
function generate_distribution() {
    var distribution_array = [];
    var bias = 0.6;
    for (var i = 0; i < 500; i++) {
        var mix = Math.random();
        distribution_array.push(Math.round(Math.random() * (1 - mix) + bias * mix));
    }
    localstorage.setItem('distribution', JSON.stringify(distribution_array));
    return distribution_array;
}
exports.default = router;
//# sourceMappingURL=login.js.map