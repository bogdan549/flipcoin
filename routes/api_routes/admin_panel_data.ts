﻿import express = require('express');
import localstorage = require("localStorage");

const router = express.Router();

router.get('/', (req: express.Request, res: express.Response) => {
    res.send({ 'users': localstorage.getItem('users') });
});

export default router;