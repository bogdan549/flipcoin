﻿import express = require('express');
import localstorage = require('localStorage');

const router = express.Router();

router.get('/', (req: express.Request, res: express.Response) => {    
    res.status(200);
    res.send({ 'distribution_array': generate_distribution() });
});

function generate_distribution() {    
    let distribution_array = [];
    let bias = 0.6;

    for (let i = 0; i < 500; i++) {        
        let mix = Math.random();

        distribution_array.push(Math.round(Math.random() * (1 - mix) + bias * mix));
    }
    localstorage.setItem('distribution', JSON.stringify(distribution_array));

    return distribution_array;    
}

export default router;