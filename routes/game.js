"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * GET game page.
 */
var express = require("express");
var router = express.Router();
router.get('/', function (req, res) {
    res.render('game');
});
exports.default = router;
//# sourceMappingURL=game.js.map