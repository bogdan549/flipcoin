"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * GET admin panel.
 */
var express = require("express");
var router = express.Router();
router.get('/', function (req, res) {
    res.render('admin_panel');
});
exports.default = router;
//# sourceMappingURL=admin_panel.js.map