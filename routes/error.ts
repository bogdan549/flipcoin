﻿/*
 * GET error page.
 */
import express = require('express');

const router = express.Router();

router.get('/', (req: express.Request, res: express.Response) => {
    res.render('error', {message: 'Sorry, service is unavailable currently, please, try later'});
});


export default router;