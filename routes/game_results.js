"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * GET game results page.
 */
var express = require("express");
var router = express.Router();
router.get('/', function (req, res) {
    if (req.query.user_amount !== 'undefined')
        res.render('game_results', { reason: req.query.reason, user_amount: req.query.user_amount });
    else
        res.render('game_results', { reason: req.query.reason });
});
exports.default = router;
//# sourceMappingURL=game_results.js.map