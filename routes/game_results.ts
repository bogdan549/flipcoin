﻿/*
 * GET game results page.
 */
import express = require('express');
const router = express.Router();


router.get('/', (req: express.Request, res: express.Response) => {
    if (req.query.user_amount !== 'undefined')
        res.render('game_results', { reason: req.query.reason, user_amount: req.query.user_amount });
    else
        res.render('game_results', { reason: req.query.reason});
});

export default router;