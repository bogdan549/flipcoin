"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * GET error page.
 */
var express = require("express");
var router = express.Router();
router.get('/', function (req, res) {
    res.render('error', { message: 'Sorry, service is unavailable currently, please, try later' });
});
exports.default = router;
//# sourceMappingURL=error.js.map