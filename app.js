"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var debug = require("debug");
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var logger = require("morgan");
var localstorage = require("localStorage");
var fs = require("fs");
//import routes 
var index_1 = require("./routes/index");
var game_1 = require("./routes/game");
var error_1 = require("./routes/error");
var login_1 = require("./routes/api_routes/login");
var turn_1 = require("./routes/api_routes/turn");
var register_1 = require("./routes/api_routes/register");
var user_result_info_1 = require("./routes/api_routes/user_result_info");
var admin_panel_data_1 = require("./routes/api_routes/admin_panel_data");
var download_user_data_1 = require("./routes/api_routes/download_user_data");
var game_results_1 = require("./routes/game_results");
var admin_panel_1 = require("./routes/admin_panel");
var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/public/', express.static(path.join(__dirname, 'public')));
app.use('/node_modules', express.static(path.join(__dirname, "node_modules")));
app.use('/', index_1.default);
app.use('/users', game_1.default);
app.use('/api/v1/register', register_1.default);
app.use('/api/v1/login', login_1.default);
app.use('/api/v1/turn', turn_1.default);
app.use('/api/v1/admin_panel_data', admin_panel_data_1.default);
app.use('/api/v1/download_user_data', download_user_data_1.default);
app.use('/error', error_1.default);
app.use('/game', game_1.default);
app.use('/game_results', game_results_1.default);
app.use('/user_result_info', user_result_info_1.default);
app.use('/admin_panel', admin_panel_1.default);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});
// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err['status'] || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
//set shared variable for all users and create file for data
localstorage.setItem('users', '{}');
localstorage.setItem('file_data', '{}');
var file = fs.createWriteStream("user_files/user_info.csv");
file.end();
app.set('port', 3000);
var server = app.listen(app.get('port'), function () {
    debug('Express server listening on port ' + server.address().port);
});
//# sourceMappingURL=app.js.map