﻿import debug = require('debug');
import express = require('express');
import path = require('path');
import bodyParser = require("body-parser");
import logger = require('morgan');
import localstorage = require("localStorage");
import fs = require("fs");

//import routes 

import index from './routes/index';
import game from './routes/game';
import error from './routes/error';

import login from './routes/api_routes/login';
import turn from './routes/api_routes/turn';
import register from './routes/api_routes/register';
import user_result_info from './routes/api_routes/user_result_info';
import admin_panel_data from './routes/api_routes/admin_panel_data';
import download_user_data from './routes/api_routes/download_user_data';

import game_results from './routes/game_results';
import admin_panel from './routes/admin_panel';

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/public/', express.static(path.join(__dirname, 'public')));
app.use('/node_modules', express.static(path.join(__dirname, "node_modules")));

app.use('/', index);
app.use('/users', game);

app.use('/api/v1/register', register);
app.use('/api/v1/login', login);
app.use('/api/v1/turn', turn);
app.use('/api/v1/admin_panel_data', admin_panel_data);
app.use('/api/v1/download_user_data', download_user_data);

app.use('/error', error);
app.use('/game', game);
app.use('/game_results', game_results);
app.use('/user_result_info', user_result_info);
app.use('/admin_panel', admin_panel);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    var err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use((err: any, req, res, next) => {
        res.status(err['status'] || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use((err: any, req, res, next) => {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

//set shared variable for all users and create file for data

localstorage.setItem('users', '{}');
localstorage.setItem('file_data', '{}');

let file = fs.createWriteStream(`user_files/user_info.csv`);                
file.end();

app.set('port', 3000);

var server = app.listen(app.get('port'), () => {
    debug('Express server listening on port ' + server.address().port);
});
